<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>이벤트 등록 관리</title>
<!-- css -->
<link rel="stylesheet" href="css/eventMain.css">
<!-- javascript -->
</head>
<body>		
            <h3>이벤트를 등록합시다.</h3>
			<ul>
			<!-- 로그아웃 후 로그인 화면으로 돌아감 -->
			<a href="logout" class=""><button>로그아웃</button></a>
			<!-- 메인으로 돌아가기 -->
			<a href="seller"><button>매장 관리 메인</button></a>
			</ul>


			<form class="events" action="./event" method="POST" enctype="multipart/form-data">
				이벤트 분류
				<!-- 이벤트 카테고리 선택하는 콤보박스 -->
				<select name="eventCombo">
					<option value="">---선택---</option>
					<c:forEach var="eventList" items="${eventCategory}">
						<option value="${eventList.eventTypeNo}">${eventList.eventTypeName}</option>
					</c:forEach>
				</select>
				이벤트 상품
				<!-- 이벤트 적용할 상품 선택하는 콤보박스 -->
				<select name="goodsCombo">
					<option value="">---선택---</option>
					<c:forEach var="goodsList" items="${goodsCategory}">
						<option value="${goodsList.goodsNo}">${goodsList.goodsName}</option>
					</c:forEach>
				</select>
				이벤트명<input type="text" name="eventName" id="" />
				할인율<input type="text" name="eventSale" id="" /><br/>
				시작일<input type="date" name="eventStdate"/>
				종료일<input type="date" name="eventEddate"/> 
				이벤트 내용 <input type="text" name="eventContent" id="" />
				이벤트 포스터<input type="file" name="eventImgUploadFile" id="" />
				<input type="hidden" name="sellerNo" id="" value="${ sellerNo }"/>
				<input type="submit" value="등록" />
			</form>
			
				<table>
				<tr>
					<th colspan="5"><h2> 이벤트 리스트 </h2></th>
				<tr>
				<tr>
					<th id="eventName" width="40%">이벤트명</th>
					<th id="eventStdate" width="15%">시작일</th>
					<th id="eventEddate" width="15">종료일</th>
					<th id="eventSale" width="15%">할인율</th>
					<th id="eventStat" width="15%">이벤트 상태</th>
				</tr>
				<c:forEach var="list" items="${eventList}">
					<tr>
						<td>${list.eventName}</td>	
						<td>${list.eventStdate}</td>
						<td>${list.eventEddate}</td>
						<td>${list.eventSale}</td>	
						<td>${list.eventStat}</td>	
					</tr>
				</c:forEach>
			</table>

</body>
</html>