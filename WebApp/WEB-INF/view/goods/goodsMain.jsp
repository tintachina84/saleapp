<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>제품 등록 관리</title>
<!-- css -->
<link rel="stylesheet" href="css/goodsMain.css">
<!-- javascript -->
</head>
<body>
			<h3>제품을 등록합시다.</h3>
			<ul>
			<!-- 로그아웃 후 로그인 화면으로 돌아감 -->
			<a href="logout" class=""><button>로그아웃</button></a>
			<!-- 메인으로 돌아가기 -->
			<a href="seller"><button>매장 관리 메인</button></a>
			</ul>
			<form class="goods" action="./goods" method="post" enctype="multipart/form-data">
				상품분류
				<!-- 상품 카테고리 선택하는 콤보박스  -->
				<select name="categoryCombo">
					<option value="">---선택---</option>
					<c:forEach var="goods" items="${category}">
						<option value="${goods.goodsTypeNo}">${goods.goodsTypeName}</option>
					</c:forEach>
				</select>
				상품명 <input type="text" name="goodsName" id="" />
				상품가격 <input type="text" name="goodsPrice" id="" />
				상품설명 <input type="text" name="goodsDesc" id="" />
				상품이미지 <input type="file" name="goodsImgUploadFile" id="" />		
				<input type="hidden" name="sellerNo" id="" value="${ sellerNo }"/>		
				<input type="submit" value="등록" />
			</form>
			
			<table>
				<tr>
					<th colspan="4"><h2> 상품 리스트 </h2></th>
				<tr>
				<tr>
					<th id="goodsName" width="25%">상품명</th>
					<th id="goodsPrice" width="15%">상품가격</th>
					<th id="goodsDesc" width="45">상품설명</th>
					<th id="goodsStat" width="15%">상품상태</th>
				</tr>
				<c:forEach var="list" items="${goodsList}">
					<tr>
						<td>${list.goodsName}</td>	
						<td>${list.goodsPrice}</td>
						<td>${list.goodsDesc}</td>
						<td>${list.goodsStat}</td>	
					</tr>
				</c:forEach>
			</table>
</body>
</html>