<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Hey!zelrut</title>
	<!-- css -->
	<link rel="stylesheet" href="css/index.css">
	<!-- javascript -->
	</head>
	
		<body>
		<!--login 전체 틀 시작 -->
		 <section class="container">
		  <!--login 시작 -->
		  <div class="login">
		      <h1>Hey!zelrut LOGIN</h1>
		      	<form action="./login" method="POST">
		      	<p>ID<br><input type="text" name="loginId" value="" placeholder="아이디" /><br></p>
				<p>PASSWORD<br><input type="password" name="loginPwd" value="" placeholder="비밀번호"/><br></p>
				<p class="submit"><input type="submit" name="commit" value="로그인"></p>
		        </form>
		  </div>
		  <!--login 끝 -->
		  <!--login 도우미 시작 --> 
		  <div class="login-help">
              <p>아이디를 잊어버리셨나요? <a href="#aa">아이디 찾기</a>.</p>
            </div>
            <!--login 도우미 끝 -->            
            <!--login 정보 시작 -->
            <div class="about">
              <p>
              &copy;헤이질럿 2015  design by.shi 
              twitter.<a href="#aa" target="">@chika15216</a>
              </p>
            </div>
           <!--login 정보 끝 -->
		 </section>
		</body>
	</html>