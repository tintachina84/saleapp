<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
	<title>매장 등록 관리</title>
	<!-- css -->
    <link rel="stylesheet" href="css/seller.css">
	<!--javascript -->
	
			
	</head>
	
		<body>
			<h3>매장을 등록합시다.</h3>
			<form action="seller" method="post">
			<ul>
			<!-- 로그아웃 후 로그인 화면으로 돌아감 -->
			<a href="logout" class=""><button>로그아웃</button></a>
			<!-- 회원 번호를 보관할 곳-->
			<input type="hidden" name="memberNo" value="${ memberNo }">
		    <input type="submit" value="매장 등록">
			</ul>
				<select name="brandNo">
					<c:forEach var="test" items="${ brand }">
						<option value="${test.brandNo }">${test.brandName }</option>
					</c:forEach>
				</select>
				<input type="text" name="sellerName" placeholder="매장명"/>
				<input type="text" name="sellerAddr" id="address" placeholder="주소를 입력하세요."/>
				<input type="text" name="sellerAddr2" id="address2" placeholder="상세 주소를 입력하세요."/>
				<input type="button" onclick="execDaumPostcode()" value="주소 검색"><br>
				
				<!-- 나중에 위치가 맞는 지 확인 할 때 사용할 지도, 단 기본적으로는 보이지 않는다. -->
				<div id="map" style="width:300px;height:300px;margin-top:10px;display:none"></div>
				
				<!-- 이후, 좌표값으로 저장할 x, y 축의 값을 hidden type 으로 저장해둔다. -->
				<input type="hidden" name="sellerLat" id="map_x">
				<input type="hidden" name="sellerLon" id="map_y">
			</form>
			<!-- 다음 우편번호 API v2 를 불러온다 -->
		<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
		<!-- 개인적으로 발급 받은 다음 맵 API, 현재는 localhost 만 사용가능하며, 이후 서버에 업로드 시 키가 변경될 수 있다. -->
		<script src="//apis.daum.net/maps/maps3.js?apikey=c277e55065ae18b5adef14c47693d9a3&libraries=services"></script>
		<script>
			/* 다음 우편번호 api + 다음 지도 api */
		    var mapContainer = document.getElementById('map'), // 지도를 표시할 div
		        mapOption = {
		            center: new daum.maps.LatLng(37.537187, 127.005476), // 지도의 중심좌표
		            level: 4 // 지도의 확대 레벨(0~14까지 있고 14는 세계지도 급일겁니다 아마)
		        };
		
		    // 지도를 미리 생성
		    var map = new daum.maps.Map(mapContainer, mapOption);
		    // 주소-좌표 변환 객체를 생성
		    var geocoder = new daum.maps.services.Geocoder();
		    // 임시 마커를 미리 생성
		    var marker = new daum.maps.Marker({
		    	// 마커의 시작 위치는 한국생산성본부
		        position: new daum.maps.LatLng(37.57490870058018, 126.97354676889898),
		        map: map
		    });
		
		    function execDaumPostcode() {
		        new daum.Postcode({
		            oncomplete: function(data) {
		                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
		                // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
		                var fullAddr = data.address; // 최종 주소 변수
		                var extraAddr = ''; // 조합형 주소 변수
		
		                // 기본 주소가 도로명 타입일때 조합한다.
		                if(data.addressType === 'R'){
		                    //법정동명이 있을 경우 추가한다.
		                    if(data.bname !== ''){
		                        extraAddr += data.bname;
		                    }
		                    // 건물명이 있을 경우 추가한다.
		                    if(data.buildingName !== ''){
		                        extraAddr += (extraAddr !== '' ? ', ' + data.buildingName : data.buildingName);
		                    }
		                    // 조합형주소의 유무에 따라 양쪽에 괄호를 추가하여 최종 주소를 만든다.
		                    fullAddr += (extraAddr !== '' ? ' ('+ extraAddr +')' : '');
		                }
		
		                // 주소 정보를 해당 필드에 넣는다.
		                document.getElementById("address").value = fullAddr;
		                // 주소로 좌표를 검색
		                geocoder.addr2coord(data.address, function(status, result) {
		                    // 정상적으로 검색이 완료됐으면
		                    if (status === daum.maps.services.Status.OK) {
		                        // 해당 주소에 대한 좌표를 받아서
		                        var coords = new daum.maps.LatLng(result.addr[0].lat, result.addr[0].lng);
		                        console.log("주소 : "+fullAddr);
		                        var map_x = document.getElementById("map_x");
		                        var map_y = document.getElementById("map_y");
		
		                        map_x.value = result.addr[0].lat;
		                        console.log("input:hidden map_x ::: "+map_x);
		                        map_y.value = result.addr[0].lng;
		                        console.log("input:hidden map_y ::: "+map_y);
		                        
		                        // 지도를 보여준다.
		                        mapContainer.style.display = "block";
		                        map.relayout();
		                        // 지도 중심을 변경한다.
		                        map.setCenter(coords);
		                        // 마커를 결과값으로 받은 위치로 옮긴다.
		                        marker.setPosition(coords)
		                    }
		                });
		             // 주소 정보를 해당 필드에 넣는다.
		             document.getElementById('address').value = fullAddr;
		
		             // 커서를 상세주소 필드로 이동한다.
		             document.getElementById('address2').focus();
		            }
		        }).open();
		    }
		</script>
			<table>	
				<c:if test="${ myStore != null }">
					<c:forEach var="test" items="${ myStore }">
						<tr>
							<td>${ test.sellerName }</td> 
							<td>${ test.sellerAddr }</td> 
							<td>${ test.sellerLat }</td> 
							<td>${ test.sellerLon }</td>
							<td class="wblue"><a href="goods?sellerNo=${ test.sellerNo }">물건 등록</a></td>
							<td class="wred"><a href="event?sellerNo=${ test.sellerNo }">이벤트 관리</a></td>
							<td><a href="">삭제</a></td>
						</tr>
					</c:forEach>		
				</c:if>
			</table>
			
		</body>

</html>