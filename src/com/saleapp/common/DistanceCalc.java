package com.saleapp.common;

public class DistanceCalc {
	
	private static double D2R = Math.PI / 180;
	
	public DistanceCalc() {
		
	}
	
	public static double caculateDistance(double lat1, double lon1, double lat2, double lon2) {
		
		double dLat = (lat2 - lat1) * D2R;
		double dLon = (lon2 - lon1) * D2R;
		
		double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(lat1 * D2R) * Math.cos(lat2 * D2R) * Math.pow(Math.sin(dLon / 2.0), 2);
		
		double c = Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) * 2;
		
		double distance = c * 6378;
		
		return distance;

	}
	
	
    public static double calDistance(double lat1, double lon1, double lat2, double lon2){  
        
        double theta, dist;  
        theta = lon1 - lon2;  
        dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1))   
              * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));  
        dist = Math.acos(dist);  
        dist = rad2deg(dist);  
          
        dist = dist * 60 * 1.1515;   
        dist = dist * 1.609344;    // 단위 mile 에서 km 변환.  
        dist = dist * 1000.0;      // 단위  km 에서 m 로 변환  
      
        return dist;  
    }  
      
        // 주어진 도(degree) 값을 라디언으로 변환  
    private static double deg2rad(double deg){  
        return (double)(deg * Math.PI / (double)180d);  
    }  
      
        // 주어진 라디언(radian) 값을 도(degree) 값으로 변환  
    private static double rad2deg(double rad){  
        return (double)(rad * (double)180d / Math.PI);  
    }
    
    
    public static String calcDistance(double lat1, double lon1, double lat2, double lon2){
        double EARTH_R, Rad, radLat1, radLat2, radDist;
        double distance, ret;

           EARTH_R = 6371000.0;
           Rad = Math.PI/180;
           radLat1 = Rad * lat1;
           radLat2 = Rad * lat2;
           radDist = Rad * (lon1 - lon2);
          
           distance = Math.sin(radLat1) * Math.sin(radLat2);
           distance = distance + Math.cos(radLat1) * Math.cos(radLat2) * Math.cos(radDist);
           ret = EARTH_R * Math.acos(distance);

           double rslt = Math.round(Math.round(ret) / 1000);
           String result = rslt + " km";
           if(rslt == 0) result = Math.round(ret) +" m";
          
           return result;
       } 
    
    
    
    public final static double AVERAGE_RADIUS_OF_EARTH = 6371;
    public static int calculateDistance(double userLat, double userLng,
      double venueLat, double venueLng) {

        double latDistance = Math.toRadians(userLat - venueLat);
        double lngDistance = Math.toRadians(userLng - venueLng);

        double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
          + Math.cos(Math.toRadians(userLat)) * Math.cos(Math.toRadians(venueLat))
          * Math.sin(lngDistance / 2) * Math.sin(lngDistance / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return (int) (Math.round(AVERAGE_RADIUS_OF_EARTH * c));
    }
    
    
    public static double distanceLatLong2(double lat1, double lng1, double lat2, double lng2) 
    {
      double earthRadius = 6371.0d; // KM: use mile here if you want mile result

      double dLat = toRadian(lat2 - lat1);
      double dLng = toRadian(lng2 - lng1);

      double a = Math.pow(Math.sin(dLat/2), 2)  + 
              Math.cos(toRadian(lat1)) * Math.cos(toRadian(lat2)) * 
              Math.pow(Math.sin(dLng/2), 2);

      double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

      return earthRadius * c; // returns result kilometers
    }

    public static double toRadian(double degrees) 
    {
      return (degrees * Math.PI) / 180.0d;
    }
}
