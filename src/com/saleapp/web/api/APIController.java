package com.saleapp.web.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.saleapp.web.login.LoginEntity;
import com.saleapp.web.member.MemberEntity;
import com.saleapp.web.test.TestEntity;
import com.saleapp.web.test.TestService;

@RestController
public class APIController {
	
	private double startPointLat = 37.5748714;
	private double startPointLon = 126.97353169999997;
	private double endPointLat = 37.5748364;
	private double endPointLon = 126.97196650000001;
	
	Logger log = Logger.getLogger(getClass());
	
	@Autowired private TestService testService;
	@Autowired private APIService apiService;
	
	@RequestMapping(value="/testAPI")
	public List<TestEntity> testAPI() {
		
		return testService.getTestJSON();
	}
	
	@RequestMapping(value="/testAPI3")
	public double testAPI3() {
		
		return apiService.getTestDistance2(startPointLat, startPointLon, endPointLat, endPointLon);
	}
	
	@RequestMapping(value="/getEventInfo", method=RequestMethod.GET)
	public APIEntity getEventInfo(HttpServletRequest request, HttpServletResponse response) {
		
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
//		int memberNo = 4;
		double lat = Double.parseDouble(request.getParameter("latitude"));
		double lon = Double.parseDouble(request.getParameter("longitude"));
		
		return apiService.getEventInfo(memberNo, lat, lon);
	}
	
	@RequestMapping(value="/getLoginInfo", method=RequestMethod.POST)
	public LoginEntity getLoginInfo(HttpServletRequest request) {
		
		String id = request.getParameter("loginId");
		String pwd = request.getParameter("loginPwd");
		
		log.debug("ID ::::::::: " + id);
		log.debug("PWD ::::::::: " + pwd);
		
		return apiService.getLoginInfo(id, pwd);
	}
	
	@RequestMapping(value="/insertMember", method=RequestMethod.POST)
	public @ResponseBody LoginEntity insertMember(HttpServletRequest request){
		String id = request.getParameter("memberId");
		String pwd = request.getParameter("memberPwd");
		String birth = request.getParameter("memberBirth");
		String phone = request.getParameter("memberPhone");
		String memberName = request.getParameter("memberName");
		
		log.debug(":::::::: memberId :::::::::" + id);
		
		MemberEntity memberEnt = new MemberEntity();
		memberEnt.setMemberId(id);
		memberEnt.setMemberPwd(pwd);
		memberEnt.setMemberBirth(birth);
		memberEnt.setMemberPhone(phone);
		memberEnt.setMemberName(memberName);
		
		return apiService.insertMember(memberEnt);
	}
	
	//관심 정보 등록
	@RequestMapping(value="/interestList", method=RequestMethod.POST)
	public int interestList(HttpServletRequest request){
		String memberNo = request.getParameter("memberNo");
		String brandNo = request.getParameter("brandNo");
		String goodsTypeNo = request.getParameter("goodsTypeNo");
		String eventTypeNo = request.getParameter("eventTypeNo");
		
		log.debug(" ::::::::::::::::::::::::::: " + brandNo + " ::::::::::::::::::::::::::: ");
		
		String[] brandNoList = brandNo.split(",");
		String[] goodsNoList = goodsTypeNo.split(",");
		String[] eventNoList = eventTypeNo.split(",");
		
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setMemberNo(memberNo);
/*		interestEnt.setBrandNo(brandNo);
		interestEnt.setGoodsTypeNo(goodsTypeNo);
		interestEnt.setEventTypeNo(eventTypeNo);*/
		
		for (int i=0; i<brandNoList.length; i++) {
			apiService.interestBrand(brandNoList[i],memberNo);
		}
		for(int i=0; i<goodsNoList.length; i++){
			apiService.interestGoods(goodsNoList[i],memberNo);
		}
		for(int i=0; i<eventNoList.length; i++){
			apiService.interestEvent(eventNoList[i],memberNo);
		}
		return 0;
	} 
	
	//관심 브랜드 ,상품, 이벤트 select
	@RequestMapping(value="/interestBrand", method=RequestMethod.POST)
	public @ResponseBody List<InterestEntity> interestBrand(HttpServletRequest request){
		String memberNo = request.getParameter("memberNo");
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setMemberNo(memberNo);
		return apiService.getInterestBrand(memberNo);
	}
	@RequestMapping(value="/interestGoods", method=RequestMethod.POST)
	public @ResponseBody List<InterestEntity> getInterestGoods(HttpServletRequest request){
		String memberNo = request.getParameter("memberNo");
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setMemberNo(memberNo);
		return apiService.getInterestGoods(memberNo);
	}
	@RequestMapping(value="/interestEvent", method=RequestMethod.POST)
	public @ResponseBody List<InterestEntity> getInterestEvent(HttpServletRequest request){
		String memberNo = request.getParameter("memberNo");
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setMemberNo(memberNo);
		return apiService.getInterestEvent(memberNo);
	}
	
	//관심 브랜드,상품,이벤트 delete
	@RequestMapping(value="/interestUpdate", method=RequestMethod.POST)
	public int interestUpdate(HttpServletRequest request){
		String memberNo = request.getParameter("memberNo");
		String brandNo = request.getParameter("brandNo");
		String goodsTypeNo = request.getParameter("goodsTypeNo");
		String eventTypeNo = request.getParameter("eventTypeNo");
		
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setMemberNo(memberNo);
				
		apiService.delInterestBrand(memberNo);
		apiService.delInterestGoods(memberNo);
		apiService.delInterestEvent(memberNo);
		
		String[] brandNoList = brandNo.split(",");
		String[] goodsNoList = goodsTypeNo.split(",");
		String[] eventNoList = eventTypeNo.split(",");
		
		for (int i=0; i<brandNoList.length; i++) {
			apiService.interestBrand(brandNoList[i],memberNo);
		}
		for(int i=0; i<goodsNoList.length; i++){
			apiService.interestGoods(goodsNoList[i],memberNo);
		}
		for(int i=0; i<eventNoList.length; i++){
			apiService.interestEvent(eventNoList[i],memberNo);
		}
		return 0;
	}
	
	@RequestMapping(value="/getEventDetail", method=RequestMethod.GET)
	public APIEventEntity getEventDetail(HttpServletRequest request) {
		int eventNo = Integer.parseInt(request.getParameter("eventNo"));
		
		return apiService.getEventDetail(eventNo);
	}
	
	@RequestMapping(value="/setMemberEvent", method=RequestMethod.POST)
	public int setMemberEvent(HttpServletRequest request) {
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
		int eventNo = Integer.parseInt(request.getParameter("eventNo"));
		return apiService.setMemberEvent(memberNo, eventNo);
	}
	
	@RequestMapping(value="/isExistMemberEvent")
	public boolean isExistMemberEvent(HttpServletRequest request) {
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
		int eventNo = Integer.parseInt(request.getParameter("eventNo"));
		return apiService.isExistMemberEvent(memberNo, eventNo);
	}
	
	@RequestMapping(value="/listMemberEvent")
	public List<String> listMemberEvent(HttpServletRequest request) {
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
		return apiService.listMemberEvent(memberNo);
	}
}
