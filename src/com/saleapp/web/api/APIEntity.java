package com.saleapp.web.api;

import java.util.ArrayList;

public class APIEntity {
	private int memberNo;
	private ArrayList<APIEventEntity> eventList;
	
	public APIEntity() {
		
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public ArrayList<APIEventEntity> getEventList() {
		return eventList;
	}

	public void setEventList(ArrayList<APIEventEntity> eventList) {
		this.eventList = eventList;
	}
	
	
}
