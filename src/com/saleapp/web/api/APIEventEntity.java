package com.saleapp.web.api;

public class APIEventEntity {
	private int eventNo;				// 이벤트번호
	private int sellerNo;				// 판매자번호
	private String eventName;			// 이벤트명
	private String eventContent;		// 이벤트내용
	private String eventStdate;			// 이벤트 시작일
	private String eventEddate;			// 이벤트 종료일
	private int eventSale;				// 할인율
	private int eventStat;				// 이벤트 상태
	private String eventImgpath;		// 이미지 경로
	private String eventGendate;		// 이벤트 생성일
	private int goodsNo;				// 제품번호
	private int eventtypeNo;			// 이벤트 타입
	private String eventtypeName;		// 이벤트 타입명
	
	private String goodsName;			// 제품명
	private int goodsPrice;				// 제품가격
	private String goodsDesc;			// 제품설명
	private String goodsImg;			// 제품이미지경로
	private String goodstypeNo;			// 제품 타입
	
	private String sellerName;			// 판매자
	private String sellerAddr;			// 판매자 주소
	private String sellerLat;			// 판매자 위도
	private String sellerLon;			// 판매자 경도
	private int brandNo;				// 브랜드 번호
	
	private String brandName;			// 브랜드명
	
	
	public APIEventEntity() {
		
	}

	public int getEventNo() {
		return eventNo;
	}

	public void setEventNo(int eventNo) {
		this.eventNo = eventNo;
	}

	public int getSellerNo() {
		return sellerNo;
	}

	public void setSellerNo(int sellerNo) {
		this.sellerNo = sellerNo;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventContent() {
		return eventContent;
	}

	public void setEventContent(String eventContent) {
		this.eventContent = eventContent;
	}

	public String getEventStdate() {
		return eventStdate;
	}

	public void setEventStdate(String eventStdate) {
		this.eventStdate = eventStdate;
	}

	public String getEventEddate() {
		return eventEddate;
	}

	public void setEventEddate(String eventEddate) {
		this.eventEddate = eventEddate;
	}

	public int getEventSale() {
		return eventSale;
	}

	public void setEventSale(int eventSale) {
		this.eventSale = eventSale;
	}

	public int getEventStat() {
		return eventStat;
	}

	public void setEventStat(int eventStat) {
		this.eventStat = eventStat;
	}

	public String getEventImgpath() {
		return eventImgpath;
	}

	public void setEventImgpath(String eventImgpath) {
		this.eventImgpath = eventImgpath;
	}

	public String getEventGendate() {
		return eventGendate;
	}

	public void setEventGendate(String eventGendate) {
		this.eventGendate = eventGendate;
	}

	public int getGoodsNo() {
		return goodsNo;
	}

	public void setGoodsNo(int goodsNo) {
		this.goodsNo = goodsNo;
	}

	public int getEventtypeNo() {
		return eventtypeNo;
	}

	public void setEventtypeNo(int eventtypeNo) {
		this.eventtypeNo = eventtypeNo;
	}

	public String getGoodsName() {
		return goodsName;
	}

	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}

	public int getGoodsPrice() {
		return goodsPrice;
	}

	public void setGoodsPrice(int goodsPrice) {
		this.goodsPrice = goodsPrice;
	}

	public String getGoodsDesc() {
		return goodsDesc;
	}

	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}

	public String getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}

	public String getGoodstypeNo() {
		return goodstypeNo;
	}

	public void setGoodstypeNo(String goodstypeNo) {
		this.goodstypeNo = goodstypeNo;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getSellerAddr() {
		return sellerAddr;
	}

	public void setSellerAddr(String sellerAddr) {
		this.sellerAddr = sellerAddr;
	}

	public String getSellerLat() {
		return sellerLat;
	}

	public void setSellerLat(String sellerLat) {
		this.sellerLat = sellerLat;
	}

	public String getSellerLon() {
		return sellerLon;
	}

	public void setSellerLon(String sellerLon) {
		this.sellerLon = sellerLon;
	}

	public int getBrandNo() {
		return brandNo;
	}

	public void setBrandNo(int brandNo) {
		this.brandNo = brandNo;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	
	public String getEventtypeName() {
		return eventtypeName;
	}

	public void setEventtypeName(String eventtypeName) {
		this.eventtypeName = eventtypeName;
	}
}
