package com.saleapp.web.api;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.saleapp.web.login.LoginEntity;
import com.saleapp.web.member.MemberEntity;

public interface APIRepository {
	public List<String> getFavorBrandInfo(int memberNo);
	public List<String> getFavorGoodstypeInfo(int memberNo);
	public List<String> getFavorEventtypeInfo(int memberNo);
	public List<APIEventEntity> getEventInfoByFavorBrand(int memberNo);
	public List<APIEventEntity> getEventInfoByFavorGoodstype(int memberNo);
	public List<APIEventEntity> getEventInfoByFavorEventtype(int memberNo);
	public LoginEntity getLoginInfo(String id, String pwd);

	public List<APIEventEntity> getEventInfoAll();
	public APIEventEntity getEventDetail(int eventNo);

	public int insertMember(MemberEntity memberEnt); //회원가입
	//관심 브랜드, 상품, 이벤트 insert
	public int interestBrand(String brandNo, String memberNo);
	public int interestGoods(String goodsTypeNo, String memberNo);
	public int interestEvent(String eventTypeNo, String memberNo);
	//관심 브랜드 ,상품, 이벤트 select
	public List<InterestEntity> getInterestBrand(String memberNo);
	public List<InterestEntity> getInterestGoods(String memberNo);
	public List<InterestEntity> getInterestEvent(String memberNo);
	//관심 브랜드,상품,이벤트 delete
	public int delInterestBrand(String memberNo);
	public int delInterestGoods(String memberNo);
	public int delInterestEvent(String memberNo);
	
	public int setEventMember(int memberNo, int eventNo);
	
	public MemberEventEntity isExistMemberEvent(int memberNo, int eventNo);
	public List<String> listMemberEvent(int memberNo);
}
