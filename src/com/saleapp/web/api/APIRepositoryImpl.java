package com.saleapp.web.api;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saleapp.web.login.LoginEntity;
import com.saleapp.web.member.MemberEntity;

@Repository
public class APIRepositoryImpl implements APIRepository {
	
	@Autowired
	private SqlSession session;
	
	@Override
	public List<String> getFavorBrandInfo(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getFavorBrand", memberNo);
	}

	@Override
	public List<String> getFavorGoodstypeInfo(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getFavorGoodstype", memberNo);
	}

	@Override
	public List<String> getFavorEventtypeInfo(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getFavorEventtype", memberNo);
	}
	
	@Override
	public List<APIEventEntity> getEventInfoByFavorBrand(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getEventInfoByFavorBrand", memberNo);
	}

	@Override
	public List<APIEventEntity> getEventInfoByFavorGoodstype(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getEventInfoByFavorGoodstype", memberNo);
	}

	@Override
	public List<APIEventEntity> getEventInfoByFavorEventtype(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.getEventInfoByFavorEventtype", memberNo);
	}

	@Override
	public LoginEntity getLoginInfo(String id, String pwd) {
		LoginEntity inputLoginInfo = new LoginEntity();
		inputLoginInfo.setMemberId(id);
		inputLoginInfo.setMemberPwd(pwd);
		return session.selectOne("com.saleapp.web.api.getLoginInfo", inputLoginInfo);
	}


	@Override
	public int insertMember(MemberEntity memberEnt) {
		return session.insert("com.saleapp.web.api.insertMember", memberEnt);
	}
	
	//관심 브랜드, 상품, 이벤트 insert
	@Override
	public int interestBrand(String brandNo, String memberNo) {
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setBrandNo(brandNo);
		interestEnt.setMemberNo(memberNo);
		return session.insert("com.saleapp.web.api.interestBrand", interestEnt);
	}
	@Override
	public int interestGoods(String goodsTypeNo, String memberNo) {
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setGoodsTypeNo(goodsTypeNo);
		interestEnt.setMemberNo(memberNo);
		return session.insert("com.saleapp.web.api.interestGoods", interestEnt);
	}
	@Override
	public int interestEvent(String eventTypeNo, String memberNo) {
		InterestEntity interestEnt = new InterestEntity();
		interestEnt.setEventTypeNo(eventTypeNo);
		interestEnt.setMemberNo(memberNo);
		return session.insert("com.saleapp.web.api.interestEvent", interestEnt);
	}

	@Override
	public List<APIEventEntity> getEventInfoAll() {
		return session.selectList("com.saleapp.web.api.getEventInfoAll");
	}

	@Override
	public APIEventEntity getEventDetail(int eventNo) {
		return session.selectOne("com.saleapp.web.api.getEventDetail", eventNo);
	}
	
	//관심 브랜드 ,상품, 이벤트 select
	@Override
	public List<InterestEntity> getInterestBrand(String memberNo) {
		return session.selectList("com.saleapp.web.api.getInterestBrand", memberNo);
	}
	@Override
	public List<InterestEntity> getInterestGoods(String memberNo) {
		return session.selectList("com.saleapp.web.api.getInterestGoods", memberNo);
	}
	@Override
	public List<InterestEntity> getInterestEvent(String memberNo) {
		return session.selectList("com.saleapp.web.api.getInterestEvent", memberNo);
	}

	//관심 브랜드,상품,이벤트 delete
	@Override
	public int delInterestBrand(String memberNo) {
		return session.delete("com.saleapp.web.api.delInterestBrand", memberNo);
	}
	@Override
	public int delInterestGoods(String memberNo) {
		return session.delete("com.saleapp.web.api.delInterestGoods", memberNo);
	}
	@Override
	public int delInterestEvent(String memberNo) {
		return session.delete("com.saleapp.web.api.delInterestEvent", memberNo);
	}

	@Override
	public int setEventMember(int memberNo, int eventNo) {
		MemberEventEntity tempEntity = new MemberEventEntity();
		tempEntity.setMemberNo(memberNo);
		tempEntity.setEventNo(eventNo);
		return session.insert("com.saleapp.web.api.setMemberEvent", tempEntity);
	}

	@Override
	public MemberEventEntity isExistMemberEvent(int memberNo, int eventNo) {
		MemberEventEntity tempEntity = new MemberEventEntity();
		tempEntity.setMemberNo(memberNo);
		tempEntity.setEventNo(eventNo);
		return session.selectOne("com.saleapp.web.api.isExistMemberEvent", tempEntity);
	}

	@Override
	public List<String> listMemberEvent(int memberNo) {
		
		return session.selectList("com.saleapp.web.api.listMemberEvent", memberNo);
	}
	
}
