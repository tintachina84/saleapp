package com.saleapp.web.api;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.saleapp.web.login.LoginEntity;
import com.saleapp.web.member.MemberEntity;

public interface APIService {
	public String getTestDistance(BigDecimal lat1, BigDecimal lon1, BigDecimal lat2, BigDecimal lon2);
	public double getTestDistance2(double lat1, double lon1, double lat2, double lon2);
	public APIEntity getEventInfo(int memberNo, double lat, double lon);
	public LoginEntity getLoginInfo(String id, String pwd);
	public LoginEntity insertMember(MemberEntity memberEnt);

	public List<APIEventEntity> getEventInfoAll();
	public APIEventEntity getEventDetail(int eventNo);

	//관심 브랜드, 상품, 이벤트 insert
	public int interestBrand(String brandNo, String memberNo);
	public int interestGoods(String goodsTypeNo, String memberNo);
	public int interestEvent(String eventTypeNo, String memberNo);
	//관심 브랜드 ,상품, 이벤트 select
	public List<InterestEntity> getInterestBrand(String memberNo);
	public List<InterestEntity> getInterestGoods(String memberNo);
	public List<InterestEntity> getInterestEvent(String memberNo);
	//관심 브랜드,상품,이벤트 delete
	public int delInterestBrand(String memberNo);
	public int delInterestGoods(String memberNo);
	public int delInterestEvent(String memberNo);
	public int setMemberEvent(int memberNo, int eventNo);
	public boolean isExistMemberEvent(int memberNo, int eventNo);
	public List<String> listMemberEvent(int memberNo);
}
