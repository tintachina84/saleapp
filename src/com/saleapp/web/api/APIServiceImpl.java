package com.saleapp.web.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saleapp.common.DistanceCalc;
import com.saleapp.web.login.LoginEntity;
import com.saleapp.web.member.MemberEntity;

@Service
public class APIServiceImpl implements APIService {
	
	private Logger log = Logger.getLogger(getClass());
	
	private static double D2R = Math.PI / 180;
	@Autowired private APIRepository apiRepository;
	
	@Override
	public String getTestDistance(BigDecimal lat1, BigDecimal lon1,
			BigDecimal lat2, BigDecimal lon2) {
		double r = 6378.1; // km
		double dLat = Math.toRadians(lat2.subtract(lat1).doubleValue());
		double dLon = Math.toRadians(lon2.subtract(lon1).doubleValue());
		double a = Math.sin(dLat/2) * Math.cos(dLat/2) 
				+ Math.cos(Math.toRadians(lat1.doubleValue())) * Math.cos(Math.toRadians(lat2.doubleValue()))
				* Math.sin(dLon/2) * Math.sin(dLon/2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d = r * c;
		
		return String.valueOf(d);
	}
	
	@Override
	public double getTestDistance2(double lat1, double lon1, double lat2,
			double lon2) {
		
		double dLat = (lat2 - lat1) * D2R;
		double dLon = (lon2 - lon1) * D2R;
		
		double a = Math.pow(Math.sin(dLat / 2.0), 2) + Math.cos(lat1 * D2R) * Math.cos(lat2 * D2R) * Math.pow(Math.sin(dLon / 2.0), 2);
		
		double c = Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) * 2;
		
		double distance = c * 6378;
		
		return distance;
	}

	@Override
	public APIEntity getEventInfo(int memberNo, double lat, double lon) {
		
		ArrayList<APIEventEntity> eventListByBrand = (ArrayList<APIEventEntity>) apiRepository.getEventInfoByFavorBrand(memberNo);
		ArrayList<APIEventEntity> eventListByGoodstype = (ArrayList<APIEventEntity>) apiRepository.getEventInfoByFavorGoodstype(memberNo);
		ArrayList<APIEventEntity> eventlistByEventtype = (ArrayList<APIEventEntity>) apiRepository.getEventInfoByFavorEventtype(memberNo);
		
		ArrayList<APIEventEntity> tempList = new ArrayList<APIEventEntity>();
		ArrayList<APIEventEntity> resultList = new ArrayList<APIEventEntity>();
		Map<Integer, APIEventEntity> tempMap = new HashMap<Integer, APIEventEntity>();
		
		for (APIEventEntity eventEntity : eventListByBrand) {
			
			tempList.add(eventEntity);
		}
		for (APIEventEntity eventEntity : eventListByGoodstype) {
			tempList.add(eventEntity);
		}
		for (APIEventEntity eventEntity : eventlistByEventtype) {
			tempList.add(eventEntity);
		}
		
		for(int i=0; i<tempList.size(); i++) {
//			apiRepository.isExistMemberEvent(memberNo, tempList.get(i).getEventNo());
			if(!tempMap.containsKey(tempList.get(i).getEventNo())) {
				double distance = DistanceCalc.caculateDistance(lat, lon, Double.parseDouble(tempList.get(i).getSellerLat()), Double.parseDouble(tempList.get(i).getSellerLon()));
				log.debug("++++++++++++++++++++++++++++++++++      " + distance + "      ++++++++++++++++++++++++++++++++++++++");
				if(distance < 0.200) {
					tempMap.put(tempList.get(i).getEventNo(), tempList.get(i));
				}
			}
		}
		
		Set<Integer> keySet = tempMap.keySet();
		Iterator<Integer> iter = keySet.iterator();
		while(iter.hasNext()) {
			resultList.add(tempMap.get(iter.next()));
		}
		
		APIEntity apiEntity = new APIEntity();
		apiEntity.setMemberNo(memberNo);
		apiEntity.setEventList(resultList);
		
		return apiEntity;
	}

	@Override
	public LoginEntity getLoginInfo(String id, String pwd) {
		
		return apiRepository.getLoginInfo(id, pwd);
	}

	@Override
	public LoginEntity insertMember(MemberEntity memberEnt) {
		apiRepository.insertMember(memberEnt);
		return apiRepository.getLoginInfo(memberEnt.getMemberId(), memberEnt.getMemberPwd());
	}

	//관심 브랜드, 상품, 이벤트 insert
	@Override
	public int interestBrand(String brandNo, String memberNo) {
		return apiRepository.interestBrand(brandNo, memberNo);
	}
	@Override
	public int interestGoods(String goodsTypeNo, String memberNo) {
		return apiRepository.interestGoods(goodsTypeNo, memberNo);
	}
	@Override
	public int interestEvent(String eventTypeNo, String memberNo) {
		return apiRepository.interestEvent(eventTypeNo, memberNo);
	}

	//관심 브랜드 ,상품, 이벤트 select
	@Override
	public List<InterestEntity> getInterestBrand(String memberNo) {
		return apiRepository.getInterestBrand(memberNo);
	}
	@Override
	public List<InterestEntity> getInterestGoods(String memberNo) {
		return apiRepository.getInterestGoods(memberNo);
	}		
	@Override
	public List<InterestEntity> getInterestEvent(String memberNo) {
		return apiRepository.getInterestEvent(memberNo);
	}

	@Override
	public List<APIEventEntity> getEventInfoAll() {
		return apiRepository.getEventInfoAll();
	}

	@Override
	public APIEventEntity getEventDetail(int eventNo) {
		return apiRepository.getEventDetail(eventNo);
	}

	
	//관심 브랜드,상품,이벤트 delete
	@Override
	public int delInterestBrand(String memberNo) {
		return apiRepository.delInterestBrand(memberNo);
	}
	@Override
	public int delInterestGoods(String memberNo) {
		return apiRepository.delInterestGoods(memberNo);
	}
	@Override
	public int delInterestEvent(String memberNo) {
		return apiRepository.delInterestEvent(memberNo);
	}

	@Override
	public int setMemberEvent(int memberNo, int eventNo) {
		return apiRepository.setEventMember(memberNo, eventNo);
	}

	@Override
	public boolean isExistMemberEvent(int memberNo, int eventNo) {
		boolean flag = false;
		MemberEventEntity booleanEnity = apiRepository.isExistMemberEvent(memberNo, eventNo);
		System.out.println(booleanEnity != null);
//		System.out.println(booleanEnity.getEventNo());
//		System.out.println(booleanEnity.getMemberNo());
		if(booleanEnity != null) {
			flag = true;
		}
		return flag;
	}

	@Override
	public List<String> listMemberEvent(int memberNo) {
		
		return apiRepository.listMemberEvent(memberNo);
	}
}










































