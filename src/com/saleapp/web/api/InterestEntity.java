package com.saleapp.web.api;

public class InterestEntity {
	private String memberNo;
	private String brandNo;
	private String goodsTypeNo;
	private String eventTypeNo;
	
	
	public InterestEntity() {

	}


	public String getMemberNo() {
		return memberNo;
	}


	public void setMemberNo(String memberNo) {
		this.memberNo = memberNo;
	}


	public String getBrandNo() {
		return brandNo;
	}


	public void setBrandNo(String brandNo) {
		this.brandNo = brandNo;
	}


	public String getGoodsTypeNo() {
		return goodsTypeNo;
	}


	public void setGoodsTypeNo(String goodsTypeNo) {
		this.goodsTypeNo = goodsTypeNo;
	}


	public String getEventTypeNo() {
		return eventTypeNo;
	}


	public void setEventTypeNo(String eventTypeNo) {
		this.eventTypeNo = eventTypeNo;
	}
}
