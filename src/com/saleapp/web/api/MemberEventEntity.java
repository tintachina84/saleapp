package com.saleapp.web.api;

public class MemberEventEntity {
	private int memberNo;
	private int eventNo;
	
	public MemberEventEntity() {
		
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}

	public int getEventNo() {
		return eventNo;
	}

	public void setEventNo(int eventNo) {
		this.eventNo = eventNo;
	}
	
	
}
