package com.saleapp.web.event;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class EventController {
	@Autowired
	EventService eventService;
	
	@RequestMapping(value="/event")
	public String event(Model model, HttpServletRequest req){		
		// 매장 번호를 얻어와서 이벤트 목록을 불러온다.
		// 현재 막는 방법이 없어서, 그냥 번호 없이 들어가면 오류가 나온다.
		int sellerNo = 0;
		if(req.getParameter("sellerNo") != null){
			sellerNo = Integer.parseInt(req.getParameter("sellerNo"));
		}else{
			sellerNo = (int) model.asMap().get("sellerNo");
		}
		
		model.addAttribute("sellerNo", sellerNo);
		model.addAttribute("eventCategory", eventService.eventCategory());
		model.addAttribute("goodsCategory", eventService.goodsCategory(sellerNo));
		model.addAttribute("eventList", eventService.selectAllEvent(sellerNo));
		return "event/eventMain";
	}
	
	@RequestMapping(value="/event", method=RequestMethod.POST)
	public String insertEvent(@ModelAttribute("eveEnt") EventEntity eveEnt,HttpServletRequest req, RedirectAttributes redirectAttribute){
		int sellerNo = Integer.parseInt(req.getParameter("sellerNo"));
		
		eventService.insertEventOk(eveEnt,req);
		redirectAttribute.addAttribute("sellerNo", sellerNo);
		
		return "redirect:event";
	}
	
	//이벤트 목록 JSON 방식으로 불러오기
	@RequestMapping(value="/eventJSON", method=RequestMethod.GET)
	public @ResponseBody List<EventEntity> eventJSON(HttpServletRequest request){
		int memberNo = Integer.parseInt(request.getParameter("memberNo"));
		return eventService.getEventJSON(memberNo);
	}
}
