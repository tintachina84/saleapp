package com.saleapp.web.event;

import java.util.List;

public interface EventRepository {
	public List<EventTypeEntity> eventTypeList();
	public List<EventGoodsEntity> eventGoodsList(int sellerNo);
	public List<EventEntity> eventList(int sellerNo);
	public int insertEvent(EventEntity eventEnt);
	public List<EventEntity> getEventJSON(int memberNo);
}
