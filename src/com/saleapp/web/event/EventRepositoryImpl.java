package com.saleapp.web.event;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EventRepositoryImpl implements EventRepository{
	@Autowired
	SqlSession session;
	
	@Override
	public List<EventTypeEntity> eventTypeList() {
		return session.selectList("com.saleapp.web.event.eventTypeList");
	}

	@Override
	public List<EventGoodsEntity> eventGoodsList(int sellerNo) {
		return session.selectList("com.saleapp.web.event.eventGoodsList", sellerNo);
	}

	@Override
	public List<EventEntity> eventList(int sellerNo) {
		return session.selectList("com.saleapp.web.event.eventList", sellerNo);
	}

	@Override
	public int insertEvent(EventEntity eventEnt) {
		return session.insert("com.saleapp.web.event.insertEvent", eventEnt);
	}

	@Override
	public List<EventEntity> getEventJSON(int memberNo) {
		return session.selectList("com.saleapp.web.event.getEventJSON", memberNo);
	}

}
