package com.saleapp.web.event;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface EventService {
	public List<EventTypeEntity> eventCategory();
	public List<EventGoodsEntity> goodsCategory(int sellerNo);
	public List<EventEntity> selectAllEvent(int sellerNo);
	public int insertEventOk(EventEntity eventEnt,HttpServletRequest req);
	public List<EventEntity> getEventJSON(int memberNo);
}
