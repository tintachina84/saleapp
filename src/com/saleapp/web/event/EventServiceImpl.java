package com.saleapp.web.event;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class EventServiceImpl implements EventService{

	@Autowired
	EventRepository eventRepository;
	
	@Override
	public List<EventTypeEntity> eventCategory() {
		return eventRepository.eventTypeList();
	}

	@Override
	public List<EventGoodsEntity> goodsCategory(int sellerNo) {
		return eventRepository.eventGoodsList(sellerNo);
	}

	@Override
	public List<EventEntity> selectAllEvent(int sellerNo) {
		return eventRepository.eventList(sellerNo);
	}

	@Override
	public int insertEventOk(EventEntity eventEnt, HttpServletRequest req) {
		MultipartFile uploadfile = eventEnt.getEventImgUploadFile();
		HttpSession session = req.getSession();
		ServletContext context = session.getServletContext();
		String filePath = context.getRealPath("/eventImgs");
		String fileName = uploadfile.getOriginalFilename();
		File file = new File(filePath+"/"+fileName);
		try {
			uploadfile.transferTo(file);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		eventEnt.setEventImgpath(fileName);
		return eventRepository.insertEvent(eventEnt);
	}

	@Override
	public List<EventEntity> getEventJSON(int memberNo) {
		return eventRepository.getEventJSON(memberNo);
	}

}
