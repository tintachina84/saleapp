package com.saleapp.web.event;

public class EventTypeEntity {
	private int eventTypeNo;
	private String eventTypeName;
		
	public EventTypeEntity() {

	}
	public int getEventTypeNo() {
		return eventTypeNo;
	}
	public void setEventTypeNo(int eventTypeNo) {
		this.eventTypeNo = eventTypeNo;
	}
	public String getEventTypeName() {
		return eventTypeName;
	}
	public void setEventTypeName(String eventTypeName) {
		this.eventTypeName = eventTypeName;
	}
	
}
