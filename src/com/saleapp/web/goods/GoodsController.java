package com.saleapp.web.goods;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.saleapp.web.event.EventEntity;

@Controller
public class GoodsController {
	@Autowired
	GoodsService goodsService;
	
	@RequestMapping(value="/goods")
	public String goods(Model model, HttpServletRequest req){
		// 매장 번호를 얻어와서 상품 목록을 불러온다.
		// 현재 막는 방법이 없어서, 그냥 번호 없이 들어가면 오류가 나온다.
		int sellerNo = 0;
		if(req.getParameter("sellerNo") != null){
			sellerNo = Integer.parseInt(req.getParameter("sellerNo"));
		}else{
			sellerNo = (int) model.asMap().get("sellerNo");
		}
		
		model.addAttribute("sellerNo", sellerNo);
		
		model.addAttribute("category", goodsService.goodscategory());
		model.addAttribute("goodsList",goodsService.selectAllGoods(sellerNo));
		return "goods/goodsMain";
	}
	
	@RequestMapping(value="/goods", method=RequestMethod.POST)
	public String insertGoods(@ModelAttribute("goodsEnt") GoodsEntity goodsEnt, HttpServletRequest req, RedirectAttributes redirectAttribute){
		
		int sellerNo = Integer.parseInt(req.getParameter("sellerNo"));
		goodsEnt.setSellerNo(sellerNo);
		goodsService.insertGoodsOk(goodsEnt,req);
		redirectAttribute.addAttribute("sellerNo", sellerNo);
		
		return "redirect:goods";
	}
	
	//이벤트 목록 JSON 방식으로 불러오기
	@RequestMapping(value="/goodsJSON", method=RequestMethod.GET)
	public @ResponseBody List<GoodsEntity> goodsJSON(){
		return goodsService.getGoodsJSON();
	}
}
