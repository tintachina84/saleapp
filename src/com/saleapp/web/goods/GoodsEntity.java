package com.saleapp.web.goods;

import org.springframework.web.multipart.MultipartFile;

public class GoodsEntity {
	private int goodsNo;
	private String goodsName;
	private int goodsPrice;
	private String goodsDesc;
	private int goodsStat;
	private String goodsGendate;
	private int sellerNo;
	private int goodstypeNo;
	private String categoryCombo; //카테고리 선택 값 받아오는 변수
	private String goodsImg;
	private MultipartFile goodsImgUploadFile;


	public GoodsEntity() {
		
	}

	public String getGoodsImg() {
		return goodsImg;
	}

	public void setGoodsImg(String goodsImg) {
		this.goodsImg = goodsImg;
	}

	public MultipartFile getGoodsImgUploadFile() {
		return goodsImgUploadFile;
	}

	public void setGoodsImgUploadFile(MultipartFile goodsImgUploadFile) {
		this.goodsImgUploadFile = goodsImgUploadFile;
	}

	public String getCategoryCombo() {
		return categoryCombo;
	}
	public void setCategoryCombo(String categoryCombo) {
		this.categoryCombo = categoryCombo;
	}
	public int getGoodsNo() {
		return goodsNo;
	}
	public void setGoodsNo(int goodsNo) {
		this.goodsNo = goodsNo;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public int getGoodsPrice() {
		return goodsPrice;
	}
	public void setGoodsPrice(int goodsPrice) {
		this.goodsPrice = goodsPrice;
	}
	public String getGoodsDesc() {
		return goodsDesc;
	}
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}
	public int getGoodsStat() {
		return goodsStat;
	}
	public void setGoodsStat(int goodsStat) {
		this.goodsStat = goodsStat;
	}
	public String getGoodsGendate() {
		return goodsGendate;
	}
	public void setGoodsGendate(String goodsGendate) {
		this.goodsGendate = goodsGendate;
	}
	public int getSellerNo() {
		return sellerNo;
	}
	public void setSellerNo(int sellerNo) {
		this.sellerNo = sellerNo;
	}
	public int getGoodstypeNo() {
		return goodstypeNo;
	}
	public void setGoodstypeNo(int goodstypeNo) {
		this.goodstypeNo = goodstypeNo;
	}
	
}
