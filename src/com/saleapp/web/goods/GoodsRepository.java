package com.saleapp.web.goods;

import java.util.List;

public interface GoodsRepository {
	public List<GoodsTypeEntity> getGoodsType();
	public List<GoodsEntity> selectGoodsList(int sellerNo);
	public int insertGoods(GoodsEntity goodsEnt);
	public List<GoodsEntity> getGoodsJSON();// 상품 목록 JSON 방식으로 불러오기
}
