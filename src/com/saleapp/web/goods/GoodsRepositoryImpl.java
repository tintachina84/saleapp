package com.saleapp.web.goods;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class GoodsRepositoryImpl implements GoodsRepository{
	@Autowired
	SqlSession session;

	@Override
	public List<GoodsTypeEntity> getGoodsType() {
		return session.selectList("com.saleapp.web.goods.getGoodsType");
	}

	@Override
	public List<GoodsEntity> selectGoodsList(int sellerNo) {
		return session.selectList("com.saleapp.web.goods.selectGoodsList", sellerNo);
	}

	@Override
	public int insertGoods(GoodsEntity goodsEnt) {
		return session.insert("com.saleapp.web.goods.insertGoods", goodsEnt);
	}
	
	// 상품 목록 JSON 방식으로 불러오기
	@Override
	public List<GoodsEntity> getGoodsJSON() {
		return session.selectList("com.saleapp.web.goods.getGoodsJSON");
	}
}
