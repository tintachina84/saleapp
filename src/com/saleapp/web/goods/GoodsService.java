package com.saleapp.web.goods;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

public interface GoodsService {
	public List<GoodsTypeEntity> goodscategory(); 
	public List<GoodsEntity> selectAllGoods(int sellerNo);
	public int insertGoodsOk(GoodsEntity goodsEnt, HttpServletRequest req);
	public List<GoodsEntity> getGoodsJSON();// 상품 목록 JSON 방식으로 불러오기
}
