package com.saleapp.web.goods;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class GoodsServiceImpl implements GoodsService{

	@Autowired
	private GoodsRepository goodsrepository;
	
	@Override
	public List<GoodsTypeEntity> goodscategory() {
		return goodsrepository.getGoodsType();
	}

	@Override
	public List<GoodsEntity> selectAllGoods(int sellerNo) {
		return goodsrepository.selectGoodsList(sellerNo);
	}

	@Override
	public int insertGoodsOk(GoodsEntity goodsEnt, HttpServletRequest req) {
		//file upload 
		MultipartFile uploadfile = goodsEnt.getGoodsImgUploadFile();
		HttpSession session = req.getSession();
		ServletContext context = session.getServletContext();
		//저장될 filePath 지정
		String filePath = context.getRealPath("/goodsImgs");
		//upload파일 파일이름 얻어오기
		String fileName = uploadfile.getOriginalFilename();
		//file경로 생성
		File file = new File(filePath+"/"+fileName);

		try {
			//선언된 파일 이름으로 파일을 upload시킴
			uploadfile.transferTo(file);
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		goodsEnt.setGoodsImg(fileName);
		return goodsrepository.insertGoods(goodsEnt);
	}
	
	// 상품 목록 JSON 방식으로 불러오기
	@Override
	public List<GoodsEntity> getGoodsJSON() {
		return goodsrepository.getGoodsJSON();
	}

}
