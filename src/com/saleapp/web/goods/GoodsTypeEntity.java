package com.saleapp.web.goods;

public class GoodsTypeEntity {
	private int goodsTypeNo;
	private String goodsTypeName;

	public GoodsTypeEntity() {
		
	}

	public int getGoodsTypeNo() {
		return goodsTypeNo;
	}

	public void setGoodsTypeNo(int goodsTypeNo) {
		this.goodsTypeNo = goodsTypeNo;
	}

	public String getGoodsTypeName() {
		return goodsTypeName;
	}

	public void setGoodsTypeName(String goodsTypeName) {
		this.goodsTypeName = goodsTypeName;
	}
	
	
}
