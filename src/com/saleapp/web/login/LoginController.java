 package com.saleapp.web.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@RequestMapping(value="/")
	public String viewIndex(HttpServletRequest request, HttpServletResponse response, Model model) {
		
		HttpSession session = request.getSession();
		String loginId = (String) session.getAttribute("memberId");
		if (loginId != null && !loginId.equals("")) {
			return "redirect:seller";
		}
		
		return "/login/index";
	}
	
	@RequestMapping(value="/login")
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = request.getSession();
		String id = request.getParameter("loginId");
		String pwd = request.getParameter("loginPwd");
		LoginEntity loginEntity = loginService.loginProcess(id, pwd);
		if (loginEntity == null) {
			return "/login/index";
		} else {
			session.setAttribute("memberId", loginEntity.getMemberId());
			session.setAttribute("memberNo", loginEntity.getMemberNo());
			//model.addAttribute("message", "로그인 성공했습니다");
			return "redirect:seller";
		}
	}
	
	@RequestMapping(value="/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response) {
		
		HttpSession session = request.getSession();
		String loginId = (String) session.getAttribute("loginId");
		if(loginId != null && !loginId.equals("")) {
			session.invalidate();
			return "/login/index";
		}
		
		return "/login/index";
	}
	
}
