package com.saleapp.web.login;

public interface LoginRepository {
	public LoginEntity getMemberInfoByIdPwd(String memberId, String memberPwd);
}
