package com.saleapp.web.login;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginRepositoryImpl implements LoginRepository {
	
	@Autowired
	SqlSession session;

	@Override
	public LoginEntity getMemberInfoByIdPwd(String memberId, String memberPwd) {
		
		return session.selectOne("com.saleapp.web.login.getMemberInfoByIdPwd", memberId);
	}
	
	
	
}
