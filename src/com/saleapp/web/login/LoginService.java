package com.saleapp.web.login;

public interface LoginService {
	public LoginEntity loginProcess(String id, String pwd);
}
