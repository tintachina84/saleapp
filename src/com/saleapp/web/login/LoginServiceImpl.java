package com.saleapp.web.login;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
	
	@Autowired
	LoginRepository loginRepository;
	
	Logger log = Logger.getLogger(LoginServiceImpl.class);
	
	@Override
	public LoginEntity loginProcess(String id, String pwd) {
		
		LoginEntity entity = loginRepository.getMemberInfoByIdPwd(id, pwd);
		if (entity == null) {
			log.debug("로그인 실패 : 아이디 존재하지 않음");
		} else {
			if (pwd.equals(entity.getMemberPwd())) {
				// 입력 아이디와 저장된 아이디의 비밀번호가 일치하는 경우
				log.debug("로그인 성공");
				return entity;
			} else {
				// 일치하지 않는 경우
				log.debug("로그인 실패 : 비밀번호 불일치");
			}
		}
		
		return null;
	}

}
