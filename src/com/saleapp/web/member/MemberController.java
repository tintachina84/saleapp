package com.saleapp.web.member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MemberController {
	@Autowired
	MemberService memberService;
	
	//회원 목록 JSON 방식으로 불러오기
	@RequestMapping(value="/memberJSON", method=RequestMethod.GET)
	public @ResponseBody List<MemberEntity> memberJSON(){
		return memberService.getMemberJSON();
	}
}
