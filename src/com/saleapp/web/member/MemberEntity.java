package com.saleapp.web.member;

public class MemberEntity {
	private int memberNo;
	private String memberId;
	private String memberPwd;
	private String memberBirth;
	private String memberPhone;
	private int memberStat;
	private String memberGendate;
	private String memberName;
	
	
	public MemberEntity() {
	}
	
	
	public MemberEntity(int memberNo, String memberId, String memberPwd,
			String memberBirth, String memberPhone, int memberStat,
			String memberGendate, String memberName) {
		super();
		this.memberNo = memberNo;
		this.memberId = memberId;
		this.memberPwd = memberPwd;
		this.memberBirth = memberBirth;
		this.memberPhone = memberPhone;
		this.memberStat = memberStat;
		this.memberGendate = memberGendate;
		this.memberName = memberName;
	}


	public int getMemberNo() {
		return memberNo;
	}
	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getMemberPwd() {
		return memberPwd;
	}
	public void setMemberPwd(String memberPwd) {
		this.memberPwd = memberPwd;
	}
	public String getMemberBirth() {
		return memberBirth;
	}
	public void setMemberBirth(String memberBirth) {
		this.memberBirth = memberBirth;
	}
	public String getMemberPhone() {
		return memberPhone;
	}
	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	public int getMemberStat() {
		return memberStat;
	}
	public void setMemberStat(int memberStat) {
		this.memberStat = memberStat;
	}
	public String getMemberGendate() {
		return memberGendate;
	}
	public void setMemberGendate(String memberGendate) {
		this.memberGendate = memberGendate;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}	
}
