package com.saleapp.web.member;

import java.util.List;

public interface MemberRepository {
	public List<MemberEntity> getMemberJSON();
}
