package com.saleapp.web.member;

import java.util.List;

import org.apache.ibatis.javassist.ClassPool;
import org.apache.ibatis.javassist.compiler.MemberResolver;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MemberRepositoryImpl implements MemberRepository{
	@Autowired
	SqlSession session;
	
	@Override
	public List<MemberEntity> getMemberJSON() {
		return session.selectList("com.saleapp.web.member.getMemberJSON");
	}


}
