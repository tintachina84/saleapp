package com.saleapp.web.member;

import java.util.List;

public interface MemberService {
	public List<MemberEntity> getMemberJSON();
}
