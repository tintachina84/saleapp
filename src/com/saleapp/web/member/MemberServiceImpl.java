package com.saleapp.web.member;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl implements MemberService{
	
	@Autowired
	MemberRepository memberregister;
	
	@Override
	public List<MemberEntity> getMemberJSON() {
		return memberregister.getMemberJSON();
	}

}
