package com.saleapp.web.schedule;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.saleapp.web.api.APIEventEntity;
import com.saleapp.web.api.APIService;

@Component
public class EventRepositoryScheduler {

	public static List<APIEventEntity> SCHEDULED_LIST;
	
	Logger logger = Logger.getLogger(getClass());
	
	@Autowired
	APIService apiService;
	
	
	public EventRepositoryScheduler() {
		
	}
	
//	@Transactional
//	@Scheduled(fixedDelay=600000)
	public void executeJob() {
		logger.debug("========================= EventRepositoryScheduler =========================");
		
		SCHEDULED_LIST = apiService.getEventInfoAll();
		
		logger.debug("============================================================================");
	}
	
}
