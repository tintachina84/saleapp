package com.saleapp.web.seller;

public class BrandEntity {
	private int brandNo;
	private String brandName;
		
	public BrandEntity() {
		super();
	}

	public BrandEntity(int brandNo, String brandName) {
		super();
		this.brandNo = brandNo;
		this.brandName = brandName;
	}

	public int getBrandNo() {
		return brandNo;
	}

	public void setBrandNo(int brandNo) {
		this.brandNo = brandNo;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
}
