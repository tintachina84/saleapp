package com.saleapp.web.seller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SellerController {
	@Autowired
	private SellerService sellerService;
	
	// 매장 기본 화면
	@RequestMapping(value="/seller")
	public String sellerView(Model model, HttpServletRequest request) {
		// session 을 사용한다.
		HttpSession session = request.getSession();
		
		// 회원의 번호가 없으면 사용할 수 없다.
		int memberNo = Integer.parseInt((String)session.getAttribute("memberNo"));
		model.addAttribute("myStore", sellerService.mySeller(memberNo));
		model.addAttribute("brand", sellerService.getBrandList());
		return "seller/seller";
	}
	
	// 매장 등록 후 보일 화면(POST와 GET의 차이이므로, 같은 페이지 이용)	
	@RequestMapping(value="/seller", method=RequestMethod.POST)
	public String sellerRegist(Model model, HttpServletRequest request, @ModelAttribute("SellerEntity")SellerEntity entity) {
	
		// 미완성인 주소와 상세 주소를 가져온다.
		String addr = entity.getSellerAddr();
		String addrDetail = request.getParameter("sellerAddr2");
		if(addrDetail != null){
			// 상세 주소가 null 이 아닐 경우, 
			// 두 주소를 합치고 entity 에 집어넣는다.
			String address = addr + " " + addrDetail;
			entity.setSellerAddr(address);
			// 그렇지 않은경우는 미완성인 주소를 그대로 집어넣으니 문제 없다.
		}
		// 데이터베이스에 값을 집어넣는다. 
		sellerService.registSeller(entity);
		
		// 다시 그 화면으로 되돌려준다.
		return "redirect:seller";
	}
	
}
