package com.saleapp.web.seller;

public class SellerEntity {
	private int sellerNo;
	private String sellerName;
	private String sellerAddr;
	private String sellerLat;
	private String sellerLon;
	private String sellerGendate;
	private int brandNo;
	private int memberNo;
	
	public SellerEntity() {}

	public SellerEntity(int sellerNo, String sellerName, String sellerAddr, String sellerLat, String sellerLon,
			String sellerGendate, int brandNo, int memberNo) {
		super();
		this.sellerNo = sellerNo;
		this.sellerName = sellerName;
		this.sellerAddr = sellerAddr;
		this.sellerLat = sellerLat;
		this.sellerLon = sellerLon;
		this.sellerGendate = sellerGendate;
		this.brandNo = brandNo;
		this.memberNo = memberNo;
	}

	public int getSellerNo() {
		return sellerNo;
	}

	public void setSellerNo(int sellerNo) {
		this.sellerNo = sellerNo;
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}

	public String getSellerAddr() {
		return sellerAddr;
	}

	public void setSellerAddr(String sellerAddr) {
		this.sellerAddr = sellerAddr;
	}

	public String getSellerLat() {
		return sellerLat;
	}

	public void setSellerLat(String sellerLat) {
		this.sellerLat = sellerLat;
	}

	public String getSellerLon() {
		return sellerLon;
	}

	public void setSellerLon(String sellerLon) {
		this.sellerLon = sellerLon;
	}

	public String getSellerGendate() {
		return sellerGendate;
	}

	public void setSellerGendate(String sellerGendate) {
		this.sellerGendate = sellerGendate;
	}

	public int getBrandNo() {
		return brandNo;
	}

	public void setBrandNo(int brandNo) {
		this.brandNo = brandNo;
	}

	public int getMemberNo() {
		return memberNo;
	}

	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}
	
	
}
