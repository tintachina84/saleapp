package com.saleapp.web.seller;

import java.util.List;

public interface SellerRepository {
	public void registSellerBySellerEntity(SellerEntity entity);
	public List<SellerEntity> mySellerByMemberNo(int memberNo);

	public List<SellerEntity> getSellerList();
	public SellerEntity getSellerOneBySellerNo(int sellerNo);
	
	public List<BrandEntity> getBrandList();
	
}
