package com.saleapp.web.seller;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SellerRepositoryImpl implements SellerRepository {
	
	@Autowired
	SqlSession sqlSession;

	@Override
	public void registSellerBySellerEntity(SellerEntity entity) {
		// 매장 등록하는 기능
		sqlSession.update("com.saleapp.web.seller.registSeller", entity);
	}
	
	@Override
	public List<SellerEntity> mySellerByMemberNo(int memberNo) {
		// 나의 매장 정보를 불러오는 기능
		return sqlSession.selectList("com.saleapp.web.seller.mySeller", memberNo);
	}

	@Override
	public List<SellerEntity> getSellerList() {
		// 전체 매장 불러오는 기능(현재 사용 안함)
		return sqlSession.selectList("com.saleapp.web.seller.getSellerList");
	}

	@Override
	public SellerEntity getSellerOneBySellerNo(int sellerNo) {
		// 매장 번호를 이용해서 매장 하나 불러오는 기능(현재 사용 안함)
		return sqlSession.selectOne("com.saleapp.web.seller.getSellerOne", sellerNo);
	}

	@Override
	public List<BrandEntity> getBrandList() {
		// 브랜드 목록을 가져오는 기능
		return sqlSession.selectList("com.saleapp.web.seller.getBrandList");
	}
}
