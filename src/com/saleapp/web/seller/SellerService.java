package com.saleapp.web.seller;

import java.util.List;

public interface SellerService {
	public void registSeller(SellerEntity entity);
	public List<SellerEntity> mySeller(int memberNo);
	
	public SellerEntity getSellerOne(int sellerNo);
	public List<SellerEntity> getSellerList();
	
	public List<BrandEntity> getBrandList();
}
