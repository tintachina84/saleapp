package com.saleapp.web.seller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SellerServiceImpl implements SellerService {
	@Autowired
	private SellerRepository sellerRepository;

	@Override
	public void registSeller(SellerEntity entity) {
		// 매장 등록하기
		sellerRepository.registSellerBySellerEntity(entity);		
	}
	
	@Override
	public List<SellerEntity> mySeller(int memberNo) {
		// 나의 매장 목록 불러오기
		return sellerRepository.mySellerByMemberNo(memberNo);
	}
	
	@Override
	public SellerEntity getSellerOne(int sellerNo) {
		// 매장 하나의 정보를 불러오기(현재 미사용)
		SellerEntity entity = sellerRepository.getSellerOneBySellerNo(sellerNo);
		return entity;
	}

	@Override
	public List<SellerEntity> getSellerList() {
		// 전체 매장 목록 불러오기(현재 미사용)
		return sellerRepository.getSellerList();
	}	
	
	@Override
	public List<BrandEntity> getBrandList() {
		// 브랜드 목록 불러오기
		return sellerRepository.getBrandList();
	}
}
