package com.saleapp.web.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {
	@Autowired
	private TestService testService;
	
	@RequestMapping(value="/test")
	public String testView(Model model) {
		model.addAttribute("model", testService.getTestId(1));
		return "test";
	}
	
	@RequestMapping(value="/testJSON", method=RequestMethod.GET)
	public @ResponseBody List<TestEntity> testJSON() {
		
		return testService.getTestJSON();
	}
}
