package com.saleapp.web.test;

public class TestEntity {
	private int testno;
	private String testid;
	
	public TestEntity() {
		
	}
	
	public void setTestno(int testno) {
		this.testno = testno;
	}
	
	public int getTestno() {
		return testno;
	}
	
	public void setTestid(String testid) {
		this.testid = testid;
	}
	
	public String getTestid() {
		return testid;
	}
}
