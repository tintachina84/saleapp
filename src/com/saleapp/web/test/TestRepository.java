package com.saleapp.web.test;

import java.util.List;

public interface TestRepository {
	public String getTestId(int testno);
	public List<TestEntity> getTestJSON();
}
