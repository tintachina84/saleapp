package com.saleapp.web.test;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TestRepositoryImpl implements TestRepository {
	
	@Autowired
	SqlSession sqlSession;
	
	@Override
	public String getTestId(int testno) {
		
		return sqlSession.selectOne("com.saleapp.web.test.getTestId", testno);
	}
	
	@Override
	public List<TestEntity> getTestJSON() {
		
		return sqlSession.selectList("com.saleapp.web.test.getTestJSON");
	}

}
