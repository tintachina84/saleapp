package com.saleapp.web.test;

import java.util.List;

public interface TestService {
	public TestEntity getTestId(int testno);
	public List<TestEntity> getTestJSON();
}
