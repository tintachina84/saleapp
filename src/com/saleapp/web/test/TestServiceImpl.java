package com.saleapp.web.test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
	@Autowired
	private TestRepository testRepository;
	
	@Override
	public TestEntity getTestId(int testno) {
		TestEntity entity = new TestEntity();
		entity.setTestid(testRepository.getTestId(testno));
		return entity;
	}
	
	@Override
	public List<TestEntity> getTestJSON() {
		
		return testRepository.getTestJSON();
	}
}
